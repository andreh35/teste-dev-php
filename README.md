## Teste para desenvolvedor PHP
Este teste faz parte do processo seletivo para a vaga de pessoa desenvolvedora na equipe que gerencia as aplicações e serviços back-end que servem a área de assinantes, aplicativo iOS e aplicativo Android.

## O desafio
A primeira parte do teste consiste na criação de um sistema de gestão de usuários, o caso de
uso é simples.
O usuário que gerenciará o sistema deverá:
* 1. Poder criar outros usuários;
* 2. Ver detalhes deste usuário recém criado;
* 3. Ver todos os usuários;
* 4. Deletar outros usuários.

Os campos mínimos necessários para que um usuário exista são: nome, avatar, email e cidade. Os campos devem ser validados corretamente.
As respostas devem ter os códigos HTTP corretos, e retornar o body em formato json.

## Estrutura do Projeto
Para desenvolvimento do projeto foi usado um contêiner docker com docker-compose que conteḿ as seguintes imagens:

* NGINX
* mysql:5.7.29
* PHP:8.0.5
* Phpmyadmin
* NPM com node:13.7
* composer
* Artisan
* MailHog

## A aplicação

* Aplicação foi desenvolvida utilizando o framework PHP Laravel 8 

## Estrutura de pastas e arquivos
    .
    ├── ...
    ├── teste-dev-php              
    │   ├── mysql                  
    │   ├── nginx                  
    │   └── project               # pasta do projeto Laravel             
    │   └── docker-compose.yml     
    │   └── docker-php-entrypoint  
    │   └── Dockerfile             
    │   └── README.md              
    └── ...

## Iniciando

### Executando a aplicação

* É necessário Baixar o projeto para um diretorio local e execuar os seguintes comandos em um terminal:
```
docker-compose up -d --build
docker-compose artisan migrate
```
* após executados os comandos acima, já é possível acessar o seu browser na porta 8084, para o projeto e 8085 para o  phpmyadmin;

 Ex:
 ```
 http://localhost:8084 [projeto]
 http://localhost/8085 [phpmyadmin]
 ```
Pode ser necessário conceder permissão de escrita na pasta ./project

## Acesso ao Banco de dados pelo Phpmyadmin
 ```
Servidor: mysql
Utilizador : testedev
Palavra-passe: testedev
 ```
## Campos que compõe o cadastro
Além dos campos necesários solicitados foram adicionados sexo e data_nascimento ao cadastro

 ```
 CAMPOS                 VALORES POSSÍVEIS
nome:                   string, min: 3, max: 50 obrigatório
avatar:                 string, min: 3, max: 50, obrigatório
email:                  string, min: 3, max: 50, unique, obrigatório
cidade:                 string, min: 3, max: 50, obrigatório
data_nascimento:        date, obrigatório, formato: YYYY-mm-dd
sexo:                   enum:Masculino,Feminino, obrigatório
 ```

## Testando os endpoints
É possível utilizar o aplicativo POSTMAN para testes das API's, para tal no campo da URL insira os valores abaixo e o tipo de requisição

 Ex:
 ```
 REQUISIÇÃO         URL                                                     PARAMS
 GET                http://localhost:8084/api/usuarios/lista                --    
 GET                http://localhost:8084/api/usuarios/lista/{id}           --
 POST               http://localhost:8084/api/usuarios/novo                 nome, email, avatar, cidade, sexo, data_nascimento
 PUT                http://localhost:8084/api/usuarios/atualiza/{id}        nome*,email*,avatar*,cidade*,sexo*,data_nascimento*
 DELETE             http://localhost:8084/api/usuarios/exclui/{id}          --         

 ```
  Obs:. Substituia o {id} pelo id so usuário cadastrado
  Legenda: *opcionais no update

## Verificando as Validações 
Para poder verificar corretamente a validação de cada campo específico no POSTMAN é necessário incluir na aba HEADERS as seguintes Keys e Values:
  ```
  KEY               VALUE
  Accept            application/json
  Content-Type      application/json
  ```

## Agradecimento
Por fim agradeço o interesse e a oportunidade de poder participar deste teste e me coloco a disposição para qualquer eventual dúvida.
Meu email: andrehilton@gmail.com




