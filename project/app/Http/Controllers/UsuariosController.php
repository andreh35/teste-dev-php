<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuarios;
use App\Http\Requests\UsuarioRequest;

class UsuariosController extends Controller
{
    public function retornaUsuario() {

        $usuarios = Usuarios::all();
        if(is_null($usuarios)) {
            return response()->json(['message' => 'Nenhum Usuário cadastrado'], 404);
        }
        return response()->json($usuarios, 200);
    }


    public function retornaUsuarioPorId($id) {
        $usuario = Usuarios::find($id);
        if(is_null($usuario)) {
            return response()->json(['message' => 'Usuário não cadastrado'], 404);
        }
        return response()->json($usuario::find($id), 200);
    }

    public function adicionaUsuario(UsuarioRequest $request) {

        $usuario = Usuarios::create($request->all());
        return response($usuario, 201);
    }

    public function atualizaUsuario(UsuarioRequest $request, $id) {

        $usuario = Usuarios::find($id);
        if(is_null($usuario)) {
            return response()->json(['message' => 'Nada a atualizar'], 404);
        }
        $usuario->update($request->all());
        return response($usuario, 200);
    }

    public function excluiUsuario(Request $request, $id) {
        $usuario = Usuarios::find($id);
        if(is_null($usuario)) {
            return response()->json(['message' => 'Usuário não foi Excluido' ], 404);
        }
        $usuario->delete();
        return response()->json(null, 204);
    }

}
