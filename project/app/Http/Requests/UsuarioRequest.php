<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [];

        switch ($this->method())
        {
        case 'POST':
            $regras = [
                //
                'nome' => ['required', 'string','min:3', 'max:50'],
                'email' => ['required', 'email','unique:usuarios'],
                'avatar' => ['required', 'string','min:3', 'max:10'],
                'sexo' => ['required',
                    Rule::in(['Masculino', 'Feminino']),
                ],
                'cidade' => ['required', 'string','min:3', 'max:50'],
                'data_nascimento' => ['required', 'date_format:Y-m-d'],
            ];

        break;
        case 'PATCH':
        case 'PUT':
            if($this->nome != ""){
                $regras['nome'] = ['required','min:3', 'max:50'];
            }
            if($this->email){
                $regras['email'] = ['required','email',
                Rule::unique('usuarios')
                ->ignore($this->id)];
            }
            if($this->avatar){
                $regras['avatar' ]= ['required','min:3', 'max:10'];
            }
            if($this->sexo){
                $regras['sexo'] = [Rule::in(['Masculino', 'Feminino'])];
            }
            if($this->cidade){
                $regras['cidade'] = ['required','min:3', 'max:50'];
            }
            if($this->data_nascimento){
                $regras['data_nascimento'] = ['required','date_format:Y-m-d'];
            }
            break;
        }
        return $regras;
    }
}
