<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'nome',
        'email',
        'avatar',
        'cidade',
        'sexo',
        'data_nascimento'
    ];
}
