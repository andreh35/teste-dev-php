<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuariosController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('usuarios/lista', [UsuariosController::class,'retornaUsuario']);
Route::get('usuarios/lista/{id}', [UsuariosController::class,'retornaUsuarioPorId']);
Route::post('usuarios/novo', [UsuariosController::class,'adicionaUsuario']);
Route::put('usuarios/atualiza/{id}', [UsuariosController::class,'atualizaUsuario']);
Route::delete('usuarios/exclui/{id}', [UsuariosController::class,'excluiUsuario']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
